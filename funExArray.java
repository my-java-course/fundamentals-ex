import java.util.Arrays;

public class funExArray {
    public static void main(String[] args) {
        int[] numbers = {2,7,4,5,1,7,18,5,10,2};
//        funMethodArray(numbers);
        System.out.println("the sum of the array is: "+ sum(numbers));
        System.out.println("the average number of the array is: "+avg(sum(numbers),numbers));
        getOdd(numbers);
        getMinMax(numbers);
        reverseArray(numbers);
        arrayOddEvenCount(numbers);
        arrayAddANumber( 7, 2,numbers);
        arrayDuplicates(numbers);
        arraySecondLargest(numbers);
        arraySumPairs(numbers, 5);
    }

    private static void arraySumPairs(int[] numbers, int i) {
        System.out.print("The pairs of numbers that sum up to "+i+" are :");
        for (int a=0;a<numbers.length;a++){
            for (int b=a+1;b<numbers.length;b++){
                if (numbers[a]+numbers[b]==i){
                    System.out.print(numbers[a]+" and "+numbers[b]+", ");
                }
            }
        }
        System.out.println();
        Arrays.sort(numbers);
        int l, r, counter = 0;
        l=0;
        r=numbers.length-1;
        while (l<=r && counter<numbers.length){
            if (numbers[l]+numbers[r]==i){
                System.out.println("this is an alternative method to find the numbers: "+numbers[l]+" and "+numbers[r]);
                l++;
                r--;
            }else if (numbers[l]+numbers[r] < i){
                l++;
                counter++;
            }else{
                r--;
                counter++;
            }
        }
    }

    private static void arraySecondLargest(int[] numbers) {
        int max = numbers[0];
        int secmax= numbers[0];
        for (int i:numbers){
            if (i>max){
                secmax=max;
                max= i;
            } else if (i>secmax){
                secmax=i;
            }
        }
        System.out.println("the second largest number in the array is: " + secmax);
    }

    private static void arrayDuplicates(int[] numbers) {
        System.out.print("similar numbers in the array are: ");
        for (int i=0;i<numbers.length-1;i++){
            for (int j=i+1;j<numbers.length;j++){
                if (numbers[i]==numbers[j] && (i!=j)){
                    System.out.print(numbers[i]+" ");
                }
            }
        }
        System.out.println();
    }

    private static void arrayAddANumber(int num, int pos,int[] numbers) {
        int[] newArr = new int[numbers.length+1];
        for (int i = 0;i<(pos-1);i++){
            newArr[i]= numbers[i];
        }
        newArr[pos-1] = num;
        for (int i = pos-1;i<numbers.length;i++){
            newArr[i+1]=numbers[i];
        }
        for (int i:newArr){
            System.out.print(i+" ");
        }
        System.out.println();
    }

    private static void arrayOddEvenCount(int[] numbers) {
        int evens = 0;
        int odds = 0;
        for (int i:numbers){
            if (i%2==0){
                evens++;
            }else{
                odds++;
            }
        }
        System.out.println("amount of even numbers in the array: "+evens);
        System.out.println("amount of odd numbers in the array: "+odds);
    }

    private static void reverseArray(int[] numbers) {
        for (int i=numbers.length-1;i>=0;i--){
            System.out.print(numbers[i] +" ");
        }
        System.out.println();
    }

    private static void getMinMax(int[] numbers) {
        int min = numbers[0];
        int max = numbers[0];
        for (int i:numbers){
            if (i<min){
                min = i;
            }
            if (i>max){
                max=i;
            }
        }
        System.out.println("smallest number in the array is: "+min);
        System.out.println("largest number in the array is: " +max);
    }

    private static void getOdd(int[] numbers) {
        System.out.print("odd numbers in the array are: ");
        for (int i:numbers){
            if (i%2!=0){
                System.out.print(i+", ");
            }
        }
        System.out.println();
    }

    private static int sum(int[] bla) {
        double sum= 0;
        for (int i:bla){
            sum += i;
        }
        return (int) sum;
    }

    private static int avg(int sum, int[] arr) {
        return sum/arr.length;
    }

    private static void funMethodArray(int[] bla) {
        double sum= 0;
        for (int i:bla){
            sum += i;
        }
        System.out.println("the sum of array is: "+sum);
        System.out.println("the average value of the elements = "+(sum/bla.length));
    }

}
